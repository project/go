<?php

class GoCacheTestCase extends GoWebTestCase {

  public function getInfo() {
    return array(
      'name' => 'Go Cache',
      'description' => "Make sure the go_cache() is working correctly.",
      'group' => 'Go Module'
    );
  }

  public function testClosure() {
    $options = array(
      'cache_id' => 'go_test:time:' . __FUNCTION__,
      'ttl' => '+ 15 minutes',
      'bin' => 'cache',
      'reset' => TRUE,
    );

    // Init the value
    $time_1 = go_cache($options, function () {
        return time();
      });
    sleep(2);

    // Call go_cache() again
    $time_2 = go_cache(array('reset' => FALSE) + $options, function () {
        return time();
      });

    // The value should be same — it's cached.
    $this->assertEqual($time_1, $time_2);
  }

  public function testStringCallback() {
    $options = array(
      'cache_id' => 'go_test:time:' . __FUNCTION__,
      'ttl' => '+ 15 minutes',
      'bin' => 'cache',
      'reset' => TRUE,
    );

    // Init the value
    $time_1 = go_cache($options, 'time');
    sleep(2);

    // Call go_cache() again
    $time_2 = go_cache(array('reset' => FALSE) + $options, 'time');

    // The value should be same — it's cached.
    $this->assertEqual($time_1, $time_2);
  }

  public function testStringCallbackWithArguments() {
    $options = array(
      'cache_id' => 'go_test:string:' . __FUNCTION__,
      'ttl' => '+ 15 minutes',
      'bin' => 'cache',
      'reset' => TRUE,
    );

    // Init the value
    $string_1 = go_cache($options, 'sprintf', array('Timestamp: %d', time()));
    sleep(2);

    // Call go_cache() again
    $string_2 = go_cache(array('reset' => FALSE) + $options, 'sprintf', array('Timestamp: %d', time()));

    // The value should be same — it's cached.
    $this->assertEqual($string_1, $string_2);
  }

  public function testObjectCallback() {
    $options = array(
      'cache_id' => 'go_test:time:' . __FUNCTION__,
      'ttl' => '+ 15 minutes',
      'bin' => 'cache',
      'reset' => TRUE,
    );

    // Init the value
    $time_1 = go_cache($options, 'GoCacheTestCase::time');
    sleep(2);

    // Call go_cache() again
    $time_2 = go_cache(array('reset' => FALSE) + $options, 'GoCacheTestCase::time');

    // The value should be same — it's cached.
    $this->assertEqual($time_1, $time_2);
  }

  /**
   * Helper method for testObjectCallback().
   * @return int
   */
  public function time() {
    return time();
  }

}
